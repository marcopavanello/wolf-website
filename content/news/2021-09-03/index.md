---
title: Releasing Real City
image: "/news/2021-09-03/background.avif"
---


Kless and I are proud to announce another assets pack, this time containing urban city models.
We have been working on this pack for some months now, starting from the traffic safeties up to the buildings.

Most of the texturing was done procedurally, this took a while since we had to reinvent the textures using the nodes but we are happy we could achieve such a high quantity of procedural materials, which also made the pack size small.

The big step up has been the recent introduction of Geometry Nodes in Blender which unlocked many new possibilities, and we couldn't waste them, this is why we built 5 procedural buildings making use of Geometry Nodes.
This allows for high flexibility to make the buildings look the way the user intends on his/her scene.

Overall it was quite a nice journey to make this pack with Kless, hope you enjoy it and learn new things exploring how we made some materials and of course the use of Geometry Nodes.
