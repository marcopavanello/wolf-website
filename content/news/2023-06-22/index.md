---
title: Assets Update
image: "/news/2023-06-22/background.avif"
---

Recently we converted all our add-ons and packs to "Assets", which means our products are now compatible with the new Blender Asset Browser.

We also unified the guide for all our products since the packs are now ready assets and require the same steps to be added in your assets library. This means you can now re-download all your purchased Wolf products and add them to your personal assets library for an easier to use experience.
