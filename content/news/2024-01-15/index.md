---
title: New assets pack - Bathrooms
image: "/news/2024-01-15/background.avif"
---

We finally released our new product, [Bathrooms](https://blendermarket.com/products/bathrooms), an assets pack of 250 interior spa and bathroom models!

[Stefan Radenkovic](https://www.artstation.com/stefan-thadeus-radenkovic) and me have been working on this project for some months now, included in the pack are showers, bathtubs, toilets, plants...  
The pack comes with some procedural models too, making use of Geometry Nodes, so you can customize them however you like.

Hope you enjoy the assets we created!
