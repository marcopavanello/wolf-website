---
title: Releasing Pebble short film assets
image: "/news/2022-01-26/background.avif"
---

It was my mistake to think the donations system would start without giving people something in return, so here we are doing the first step, we are now releasing the [Pebble](/films/pebble) short film project assets under the CC-0 license.

Enjoy the project files, listen to the soundtrack and how it has been composed, discover how the sets and characters were modeled...
We hope you will find the assets useful for your projects too.
