---
title: New short film - The Spark
image: "/news/2023-11-24/background.avif"
---

Today we finally released our second short film [The Spark](/films/the_spark) made entirely with Blender!

What a journey it has been, since we wrote the story back in 2021 up to the final release. Sci-fi can be interesting if made with purpose and thought, what we wanted to show with the film was a post-humanity world, where nature has died everywhere except in one place, and that place is being protected by some robots. I guess the film can be interpreted in many ways, but if you haven't already then give it a watch, I hope the around 12 minutes of duration don't bore you.  
[Joseph Dowling](https://www.artstation.com/joedowling) did the previz in 2022, and that gave me many ideas as well as a proof of concept to base all the shots on.

## Assets
I got some very highly talented artists to work in the film, [Josue Rivera](https://josuer3d.com/) modeled the robot protagonist while [Bernardo Iraci](https://mstdn.plus/@bernardo) did the Greenland set, needless to say they did an awesome job!

We made use of some of our asset packs, like for the city we used the [Real City](https://blendermarket.com/products/real-city) models and made them procedurally destroyable using Geometry Nodes, also we used the [Real Environments](https://blendermarket.com/products/real-environments) procedural desert for the desert scenes, and then the procedural dust and ivy from [Generators](/assets) for the giant robots and the city scenes.
We made use of procedural textures as much as possible, but we also used textures and assets from resources like [Polyhaven](https://polyhaven.com/) and [AmbientCG](https://ambientcg.com/).

## Geometry
Geometry Nodes were basically used in every scene, from the destroyable city buildings to the scattering of debris and rubbish around the streets, also the city gate and walls were made using Geometry Nodes, the dust particles in the interior scene, the fires in the battle scene, the desert displacement, for the scattering of dead robots pieces, for the scattering of rocks and grass...

We made extensive use of Simulation Nodes too, we used them for the shooting and the sparks in the giants robots' battle scene, for the pebbles falling down the sewerage tunnel, we used them for the rain and rain bounces in the cave scene, we also used them for some floating particles in the sandstorm scenes.

## Sound
As for the audio we made use of sounds from [Freesound](https://freesound.org/), and for the music I composed the pieces in such a way to evoke an emotional response, while still being glued to the visuals, all by using LMMS, a free and open source software.

The dedication that went into the short film is worth more than 1 year of work, by far my longest project, hope you enjoy it.
