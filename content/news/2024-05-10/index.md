---
title: New Grass generator
image: "/news/2024-05-10/background.avif"
---

[Grass](https://blendermarket.com/products/grass-gen) is a procedural grass generator system. It comes in handy to anyone who needs to add vegetation to their scenes.

We studied grass down to the millimeter to reproduce real grass and weeds with accurate height and physically based materials. From the dead grass on the ground, to the leaves, up to the different high stems, this grass generator comes with 12 Grass species and 8 Weed species.

Being it 100% based on Geometry Nodes the system gives you full control over your lawns. With the slide of a Seed parameter, you get infinite random variations of the same grass or weed species.
No models, no image textures, everything is generated on the fly!