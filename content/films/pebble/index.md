---
title: Pebble
year: 2020
poster: /films/pebble/images/poster.avif
---


<div class="background">
    <div class="filter">
        <img src="images/title.svg">
    </div>
</div>

<div class="plot border">
    <p>Pebble, a unique caterpillar made of pebbles, is determined to reach a green leaf</p>
</div>

<div class="space"></div>

{{< video url="https://makertube.net/videos/embed/dd10611a-3575-4e39-8bbb-6d016235f581" >}}

<div class="space"></div>

<div class="infobox border">
    <div class="column">
        <div class="left">
            <p>Year :</p>
            <p>Duration :</p>
            <p>License :</p>
            <p>Project :</p>
        </div>
    </div>
    <div class="column">
        <p>2020</p>
        <p>5 min 7 sec</p>
        <p>CC-BY</p>
        <p>
			<a href="https://mega.nz/folder/9MxDCIzI#Oqxy1UMH7MUASMrxko7yeA">Open</a>
		</p>
    </div>
</div>

<div class="space"></div>

## Credits

<div class="credits border">
    <p>{{< collaborators member="marco" >}}: Director, Producer, Music, Modeling, Animation</p>
    <p>{{< collaborators member="joseph" >}}: Caterpillar rigger, General advisor</p>
    <p>{{< collaborators member="nathan" >}}: Pebble design, Story advisor</p>
</div>

<div class="space"></div>

## Software used

<div class="items software border">
    <div class="item">
        <img src="/logos/Blender.svg">
        <p>Blender</p>
    </div>
    <div class="item">
        <img src="/logos/LMMS.svg">
        <p>LMMS</p>
    </div>
    <div class="item">
        <img src="/logos/Audacity.svg">
        <p>Audacity</p>
    </div>
    <div class="item">
        <img src="/logos/Krita.svg">
        <p>Krita</p>
    </div>
    <div class="item">
        <img src="/logos/GIMP.svg">
        <p>GIMP</p>
    </div>
    <div class="item">
        <img src="/logos/Meshroom.svg">
        <p>Meshroom</p>
    </div>
</div>

<div class="space"></div>

## Making of

This is our first short film and it's completely made with Blender.  
For this film we used only free software since it's possible to make content with free software only.  
Some assets were ready (like the trees from our [Real Trees](https://blendermarket.com/products/real-trees) assets pack), but we still had to make the microscopic pebbles, leaves and other details in order to provide the touch of realism that we based the film on.
{.paragraph}

<div class="space-small"></div>

{{< video url="https://makertube.net/videos/embed/263fbf3c-2818-4e57-a0f9-c687876410fe" >}}
