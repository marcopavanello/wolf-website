---
title: Making Real Sky open source
image: "/news/2022-08-10/background.avif"
---

4 years passed since the release of the [Real Sky](https://gitlab.com/marcopavanello/real-sky) add-on, we think it's finally time to make it available to everyone.

We can't thank you all enough for the help and support you gave to us, this started as a project to make realistic illumination in Blender and it gained some kind of popularity thanks to you.

The Sky Texture in Blender kind of replaced the purpose of this add-on in providing realistic lighting, but this add-on still has the procedural clouds which could be used to populate the sky dome, we hope everyone can benefit from it now that we decided to release it as open source.

Have fun!
