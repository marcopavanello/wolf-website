---
title: New Ocean generator
image: "/news/2024-12-21/background.avif"
---

[Ocean](https://blendermarket.com/products/ocean) is a procedural oceans generator system. It allows to easily create various different oceans suitable for your scenes.

Oceans can always be faked with shaders, but this time we wanted to achieve something more detailed with real displacement and foam. Using the Gerstner waves we could build through the use of Geometry Nodes a procedural and completely customizable ocean.
