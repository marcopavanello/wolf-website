# website
This is our [website](https://wolfstudio.io/) based on [Hugo](https://gohugo.io/) static site generator
<div align="center">
    <img src="static/logos/wolf_theme.svg" width="256px" height="256px"/>
</div>
