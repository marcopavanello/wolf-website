---
title: Bye products, welcome Assets
image: "/news/2024-01-05/background.avif"
---

What a better way to start the year than with a decluttering of the whole website!

We finally got rid of the Product pages, yes for real, basically we used to have a duplicate page of each product from the Blender Market in this website, not anymore.
We replaced the Products page with the new [Assets](/assets) one, providing a link to the [Blender Market](https://blendermarket.com/creators/wolf) and a better way to discover all the free assets we offer.  
This was a needed change since having a duplication of all our products was really unnecessary, our paid products are available on Blender Market while we let the free ones be on our website.
