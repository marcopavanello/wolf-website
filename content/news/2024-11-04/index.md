---
title: New Trees generator
image: "/news/2024-11-04/background.avif"
---

[Trees](https://blendermarket.com/products/trees) is a procedural trees generator system. It allows to easily create various different trees suitable for your scenes.

To make physically accurate trees in Blender has always been a challenge, and finally with the new Geometry Nodes tools we were able to achieve realistic and natural results. From the main trunk to the branches and leaves, this generator uses some complex algorithms to maintain a natural and physically accurate structure.

Being it 100% based on Geometry Nodes the system gives you full control over the trees. With the slide of a Seed parameter you get an infinite amount of variations of the same type of tree.
No models or image textures, everything is generated on the fly!