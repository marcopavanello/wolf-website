---
title: Assets
description: Free and paid Blender assets
stylesheets: ["/styles/style_assets.css"]
---

<div class="background">
    <div class="filter">
        <div class="banner">
            <h2>Discover our products</h2>
            <div class="buttons">
                <a class="button" href="https://blendermarket.com/creators/wolf">Blender Market</a>
                <a class="button" href="https://wolfstudioart.gumroad.com/">Gumroad</a>
                <a class="button" href="https://drive.proton.me/urls/S9ZBEQW8A4#CxJFlWT93kh0">Free Assets</a>
            </div>
        </div>
    </div>
</div>

<div class="space"></div>

<div class="advertisement">
    <div class="faderight">
        <div class="fade">
            <div class="rightside">
                <div class="description border">
                    <p>Years in the making, we provide high quality and future proof Blender assets.</p>
                </div>
            </div>
            <img src="/assets/bathrooms.avif">
        </div>
    </div>
    <div class="space"></div>
    <div class="fadeleft">
        <div class="fade">
            <div class="leftside">
                <div class="description border">
                    <p>From the smallest strands of grass to the famous Japanese temples... We got everything covered.</p>
                </div>
            </div>
            <img src="/assets/grass.avif">
        </div>
    </div>
    <div class="space"></div>
    <div class="faderight">
        <div class="fade">
            <div class="rightside">
                <div class="description border">
                    <p>Procedural assets, built with scalability in mind.</p>
                </div>
            </div>
            <img src="/assets/rocks.avif">
        </div>
    </div>
    <div class="space"></div>
    <div class="fadeleft">
        <div class="fade">
            <div class="leftside">
                <div class="description border">
                    <p>Our tools have been helpful to thousands of artists all around the world.</p>
                </div>
            </div>
            <img src="/assets/environments.avif">
        </div>
    </div>
</div>
