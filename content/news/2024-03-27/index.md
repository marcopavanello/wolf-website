---
title: Landing on Gumroad
image: "/news/2024-03-27/background.avif"
---

With the introduction of a "Collaborators" feature on Gumroad, we could finally add our products and create our own [Gumroad Store](https://wolfstudioart.gumroad.com/).  
This new store is an exact copy of the [Blender Market Store](https://blendermarket.com/creators/wolf), but it allows everyone without a Blender Market account to buy our assets.
