---
title: The Spark
year: 2023
poster: /films/the_spark/images/poster.avif
---


<div class="background">
    <div class="filter">
        <img src="images/title.svg">
    </div>
</div>

<div class="plot border">
    <p>Humanity is long gone. After an apocalyptic war, a member of the team of robots tasked with finding a new hope for life discovers a mysterious map</p>
</div>

<div class="space"></div>

<div class="short">
    {{< video url="https://makertube.net/videos/embed/c8fff8d2-9186-496f-b964-389312a2c478" >}}
</div>

<div class="space"></div>

<div class="infobox border">
    <div class="column">
        <div class="left">
            <p>Year :</p>
            <p>Duration :</p>
			<p>License :</p>
            <p>High Quality :</p>
        </div>
    </div>
    <div class="column">
        <p>2023</p>
        <p>11 min 45 sec</p>
		<p>CC-BY</p>
        <p>
			<a href="https://mega.nz/folder/4FATxbgK#-r3NisUO6TC3j-PhzBeejw">Download</a>
		</p>
    </div>
</div>

<div class="space"></div>

## Credits

<div class="credits border">
    <p>{{< collaborators member="marco" >}}: Director, Producer, Music, Modeling, Animation</p>
    <p>{{< collaborators member="bernardo" >}}: Greenland set</p>
    <p>{{< collaborators member="joseph" >}}: Previz, General Advisor</p>
    <p>{{< collaborators member="josue" >}}: Robot Protagonist</p>
    <p>{{< collaborators member="nathan" >}}: General Advisor</p>
    <p>{{< collaborators member="stefan" >}}: General Advisor</p>
</div>

<div class="space"></div>

## Software used

<div class="items software border">
    <div class="item">
        <img src="/logos/Blender.svg">
        <p>Blender</p>
    </div>
    <div class="item">
        <img src="/logos/LMMS.svg">
        <p>LMMS</p>
    </div>
    <div class="item">
        <img src="/logos/Tenacity.svg">
        <p>Tenacity</p>
    </div>
    <div class="item">
        <img src="/logos/GIMP.svg">
        <p>GIMP</p>
    </div>
</div>

<div class="space"></div>

## Making of

This is our second short film completely made with Blender and free and open source software.  
We made use of our [Real City](https://blendermarket.com/products/real-city) assets as well as [Real Environments](https://blendermarket.com/products/real-environments) and [Generators](/assets/), but the rest of the models had to be made from scratch.  
The whole project is worth more than one year of work, and this time around we got some highly talented artists involved.
{.paragraph}

<div class="space-small"></div>

{{< video url="https://makertube.net/videos/embed/c26f69c6-119e-404d-89e6-7d259a52b417" >}}
