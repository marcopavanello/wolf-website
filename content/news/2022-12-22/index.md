---
title: A new assets pack, about Japan
image: "/news/2022-12-22/background.avif"
---

After months of work, we are finally at the end of the year releasing a new pack, this time about Japan ([Japanese Architecture](https://blendermarket.com/products/japanese-architecture)).  
Containing 6 of the most known Japanese temples this pack comes ready for the Blender Asset Browser, meaning you get a nice way to import the models to your scene. The roofs have been the most challenging part to get right in this project, as we made use of Geometry Nodes to make them fully procedural with the tiles and stuff. Also some materials are made procedurally with the shaders, while for others we made use of CC-0 textures.  
Enjoy the pack!
