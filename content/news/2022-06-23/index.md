---
title: Releasing Generators
image: "/news/2022-06-23/background.avif"
---

I love to see the amazing works by artists around the web creating the impossible with Geometry Nodes, and I am really grateful they share their experience on how they made them too.

So, I thought it would be fun to follow some awesome tutorials and release the blend files to the public, so here we are, a pack of 16 Geometry Nodes [Generators](/assets), completely procedural and customizable by the user.

This pack comes in the shape of assets for Blender, meaning you can place the files inside your assets library directory and you will be able to import/append them by drag and dropping.

What's in the pack? As of now it comes with a bridge, a spider web, a tree, and other cool stuff, but we plan to expand it in the future.

With this release, we are also merging the "PBR Materials" add-on to [Material Assets](/assets), again in the shape of an assets library.
